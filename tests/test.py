import unittest
import base64
import binascii
import schnorrpy

class TestKeypair(unittest.TestCase):
    def test_keypair_from_seed(self):
        """Test createing a keypair from a 32 byte seed"""
        # Define our seed, and comparison data
        seed_hex = (
            b'0a8db9cd8fa3c74cfd55839175a255a164d1db2d6fd128efb4ddeec35e29f2ed'
        )
        expected_pub_hex = (
            b'56a1edb23ba39364bca160b3298cfb7ec8c5272af841c59a7160123a7d705c4d'
        )
        expected_private_hex = (b'206882f2fb31a37c64cda4337625ae71ec96f133747c'
                                b'2a79e2d7909825e7d15261236a3191bca4e5ef5fe961'
                                b'4f0403104087d5a39fcfcee40060bdc9b3c19673')
        # unhex our seed value
        seed = binascii.unhexlify(seed_hex)
        # use schnorrpy to generate our keypair
        keypair = schnorrpy.keypair_from_seed(seed)

        self.assertEqual(binascii.hexlify(keypair[1]), expected_private_hex)
        self.assertEqual(binascii.hexlify(keypair[0]), expected_pub_hex)

    def test_sign(self):
        """Test that we can successfully sign a message"""
        keypair = (
            binascii.unhexlify(b'56a1edb23ba39364bca160b3298cfb7ec8c5272af841c'
                b'59a7160123a7d705c4d'),
            binascii.unhexlify(b'206882f2fb31a37c64cda4337625ae71ec96f133747c'
                b'2a79e2d7909825e7d15261236a3191bca4e5ef5fe961'
                b'4f0403104087d5a39fcfcee40060bdc9b3c19673')
        )

        message = base64.b64encode(
            b'it reaches out it reaches out it reaches out it reaches out'
            b'One hundred and thirteen times a second, nothing answers and it'
            b'reaches out. It is not conscious, though parts of it are.'
            b'There are structures within it that were once separate'
            b' organisms; aboriginal, evolved, and complex. It is designed to'
            b' improvise, to use what is there and then move on. Good enough '
            b'is good enough, and so the artifacts are ignored or adapted. '
            b'The conscious parts try to make sense of the reaching out.'
            b'Try to interpret it.')
        sig = schnorrpy.sign(keypair, message)

    def test_verify_valid_signature(self):
        """Test that we can verify a valid signature"""
        publicKey = binascii.unhexlify(
                b'56a1edb23ba39364bca160b3298cfb7ec8c5272af841c'
                b'59a7160123a7d705c4d')
        sig = binascii.unhexlify(b'bcb041916b70af03b17ed1c622817ffa50815499dbe18895d2f9618f006ab'
            b'64886c69d5a20e04ff66317c7e14d2580dedfc6bbff14c380569cad5c02719c'
            b'420c')

        message = base64.b64encode(
            b'it reaches out it reaches out it reaches out it reaches out'
            b'One hundred and thirteen times a second, nothing answers and it'
            b'reaches out. It is not conscious, though parts of it are.'
            b'There are structures within it that were once separate'
            b' organisms; aboriginal, evolved, and complex. It is designed to'
            b' improvise, to use what is there and then move on. Good enough '
            b'is good enough, and so the artifacts are ignored or adapted. '
            b'The conscious parts try to make sense of the reaching out.'
            b'Try to interpret it.')
        self.assertTrue(schnorrpy.verify(sig, message, publicKey))

    def test_fail_verify_invalid_signature(self):
        """Test that a invalid signature will fail to verify"""
        publicKey = binascii.unhexlify(
            b'56a1edb23ba39364bca160b3298cfb7ec8c5272af841c'
            b'59a7160123a7d705c4d')
        sig = binascii.unhexlify(b'bcb041916b70af03b17ed1c622817ffa50815499db'
            b'e18895d2f9618fdeadbeef86c69d5a20e04ff66317c7e14d2580dedfc6bbff1'
            b'4c380569cad5c02719c420c')

        message = base64.b64encode(
            b'it reaches out it reaches out it reaches out it reaches out'
            b'One hundred and thirteen times a second, nothing answers and it'
            b'reaches out. It is not conscious, though parts of it are.'
            b'There are structures within it that were once separate'
            b' organisms; aboriginal, evolved, and complex. It is designed to'
            b' improvise, to use what is there and then move on. Good enough '
            b'is good enough, and so the artifacts are ignored or adapted. '
            b'The conscious parts try to make sense of the reaching out.'
            b'Try to interpret it.')
        self.assertFalse(schnorrpy.verify(sig, message, publicKey))
